//
//  ISAppDelegate.h
//  Support
//
//  Created by Сергей Костян on 12.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISSupport.h"

@interface ISAppDelegate : UIResponder <UIApplicationDelegate,ISSupportUIDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
