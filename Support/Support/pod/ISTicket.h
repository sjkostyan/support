//
//  ISTicket.h
//  Support
//
//  Created by Сергей Костян on 12.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISTicket : NSObject

@property (strong) NSString *ticketID;
@property (strong) NSString *title;
@property (strong) NSString *status;
@property (strong) NSString *messagesCount;

-(id)initWithDictionary:(NSDictionary *)object;

@end
