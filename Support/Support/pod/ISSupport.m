//
//  SKDataManager.m
//  MensLook
//
//  Created by Сергей Костян on 23.04.14.
//  Copyright (c) 2014 Сергей Костян. All rights reserved.
//

#import "ISSupport.h"

@implementation ISSupport

#pragma mark - singleton -

static ISSupport *DataManager = nil;

+(ISSupport *)defaultManager
{
    if(DataManager == nil)
    {
        DataManager = [[ISSupport alloc] init];
        [DataManager initVC];
        [DataManager getDeviceToken];
    }
    return DataManager;
}

#pragma mark - main vc -

-(void)initVC
{
    _ticketVC = [[ISTicketsVC alloc] initWithNibName:@"ISTicketsVC" bundle:nil];
}

#pragma mark - device UUID -

-(void)getDeviceToken
{
    _deviceToken = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

#pragma mark - gettres -

-(ISTicketsVC *)ticketVC
{
    return _ticketVC;
}

-(NSSet *)subjects
{
    return [NSSet setWithObjects:@"1",@"2",@"3", nil];
}

#pragma mark - font -

-(void)setHeaderFont:(UIFont *)font
{
    _headerFont = font;
}

-(UIFont *)headerFont
{
    if(!_headerFont)
        return [UIFont boldSystemFontOfSize:17.];
    return _headerFont;
}

#pragma mark - text color -

-(void)setHeaderTextColor:(UIColor *)color
{
    _headerTextColor = color;
}

-(UIColor *)headerTextColor
{
    if(!_headerTextColor)
        return [UIColor blackColor];
    return _headerTextColor;
}

#pragma mark - status bar -

static UIStatusBarStyle _style = UIStatusBarStyleLightContent;

+(void)statusBarStyle:(UIStatusBarStyle)style
{
    _style = style;
}
+(UIStatusBarStyle)statusBarStyle
{
    return _style;
}

#pragma mark - set titles -

//titles
static NSString *_ticketsListHeader = @"Tickets";
static NSString *_alertTitle = @"New ticket";
static NSString *_alertMessage = @"Select ticket type";
static NSString *_alertCancelButtonTitle = @"Cancel";
static NSString *_newTicketBackButtonTitle = @"Reject";
static NSString *_newTicketSendButtonTitle = @"Send";
static NSString *_newTicketSubjectTitle = @"Subject";
static NSString *_ticketMessagesBackButtonTitle = @"Back";
static NSString *_ticketMessagesSendButtonTitle = @"Send";
static NSString *_ticketMessagesPlaceholder = @"Type something...";
static NSString *_administratorDisplayName = @"Administrator";
static NSString *_userDisplayName = @"You";

+(void)setTicketsListHeader:(NSString *)title
{
    _ticketsListHeader = title;
}

+(void)setAlertTitle:(NSString *)title
{
    _alertTitle = title;
}

+(void)setAlertMessage:(NSString *)title
{
    _alertMessage = title;
}

+(void)setAlertCancelButtonTitle:(NSString *)title
{
    _alertCancelButtonTitle = title;
}

+(void)setNewTicketBackButtonTitle:(NSString *)title
{
    _newTicketBackButtonTitle = title;
}

+(void)setNewTicketSendButtonTitle:(NSString *)title
{
    _newTicketSendButtonTitle = title;
}

+(void)setNewTicketSubjectTitle:(NSString *)title
{
    _newTicketSubjectTitle = title;
}

+(void)setTicketMessagesBackButtonTitle:(NSString *)title
{
    _ticketMessagesBackButtonTitle = title;
}

+(void)setTicketMessagesSendButtonTitle:(NSString *)title
{
    _ticketMessagesSendButtonTitle = title;
}

+(void)setTicketMessagesPlaceholder:(NSString *)title
{
    _ticketMessagesPlaceholder = title;
}

+(void)setAdministratorDisplayName:(NSString *)title
{
    _administratorDisplayName = title;
}

+(void)setUserDisplayName:(NSString *)title
{
    _userDisplayName = title;
}

#pragma mark - get titles -

+(NSString *)ticketsListHeader
{
    return  _ticketsListHeader;
}

+(NSString *)alertTitle
{
    return _alertTitle;
}

+(NSString *)alertMessage
{
    return _alertMessage;
}

+(NSString *)alertCancelButtonTitle
{
    return _alertCancelButtonTitle;
}

+(NSString *)newTicketBackButtonTitle
{
    return _newTicketBackButtonTitle;
}

+(NSString *)newTicketSendButtonTitle
{
    return _newTicketSendButtonTitle;
}

+(NSString *)newTicketSubjectTitle
{
    return _newTicketSubjectTitle;
}

+(NSString *)ticketMessagesBackButtonTitle
{
    return _ticketMessagesBackButtonTitle;
}

+(NSString *)ticketMessagesSendButtonTitle
{
    return _ticketMessagesSendButtonTitle;
}

+(NSString *)ticketMessagesPlaceholder
{
    return _ticketMessagesPlaceholder;
}

+(NSString *)administratorDisplayName
{
    return [_administratorDisplayName stringByAppendingString:@" :"];
}

+(NSString *)userDisplayName
{
     return [_userDisplayName stringByAppendingString:@" :"];
}

#pragma mark - delegate button customizing -

-(UIBarButtonItem *)_viewForAddButton
{
    if([_delegate respondsToSelector:@selector(viewForAddButton)])
    {
        return [_delegate viewForAddButton];
    }
    return nil;
}

-(UIBarButtonItem *)_viewForBackButtonInNewTicketView
{
    if([_delegate respondsToSelector:@selector(viewForBackButtonInNewTicketView)])
    {
        return [_delegate viewForBackButtonInNewTicketView];
    }
    return nil;
}

-(UIBarButtonItem *)_viewForSendButtonInNewTicketView
{
    if([_delegate respondsToSelector:@selector(viewForSendButtonInNewTicketView)])
    {
        return [_delegate viewForSendButtonInNewTicketView];
    }
    return nil;
}

-(UIBarButtonItem *)_viewForBackButtonInTicketMessagesView
{
    if([_delegate respondsToSelector:@selector(viewForBackButtonInTicketMessagesView)])
    {
        return [_delegate viewForBackButtonInTicketMessagesView];
    }
    return nil;
}

//-(UIButton *)_viewForSendButtonInTicketMessagesView
//{
//    if([_delegate respondsToSelector:@selector(viewForSendButtonInTicketMessagesView)])
//    {
//        return [_delegate viewForSendButtonInTicketMessagesView];
//    }
//    return nil;
//}

#pragma mark - button customizing -

+(UIBarButtonItem *)viewForAddButton
{
    return [DataManager _viewForAddButton];
}

+(UIBarButtonItem *)viewForBackButtonInNewTicketView
{
    return [DataManager _viewForBackButtonInNewTicketView];
}

+(UIBarButtonItem *)viewForSendButtonInNewTicketView
{
    return [DataManager _viewForSendButtonInNewTicketView];
}

+(UIBarButtonItem *)viewForBackButtonInTicketMessagesView
{
    return [DataManager _viewForBackButtonInTicketMessagesView];
}

//+(UIButton *)viewForSendButtonInTicketMessagesView
//{
//    return [DataManager _viewForSendButtonInTicketMessagesView];
//}

@end
