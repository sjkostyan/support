//
//  ISTicketVC.h
//  Support
//
//  Created by Сергей Костян on 12.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISTicketsVC : UITableViewController <UIAlertViewDelegate>
{
    NSArray *_tickets;
    
    UIRefreshControl *_resfreshControl;
}

-(void)_downloadTicketsList;

@end

@interface ISTicketsCell : UITableViewCell

@property (strong) IBOutlet UILabel *ticketTitle;
@property (strong) IBOutlet UILabel *ticketStatus;
@property (strong) IBOutlet UILabel *ticketMessagesCount;

@property (strong) IBOutlet UIImageView *ticketStatusImage;

@end
