//
//  ISTicketForm.m
//  Support
//
//  Created by Сергей Костян on 12.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import "ISTicketForm.h"

#define kNewTicketEndpoint @"/api/ticket/new"

@interface ISTicketForm ()

@end

@implementation ISTicketForm

#pragma mark - status bar -

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return [ISSupport statusBarStyle];
}

#pragma mark - view -

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    UILabel *titleView = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., 320., 40.)];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [[ISSupport defaultManager] headerTextColor];
    titleView.text = [ISSupport alertTitle];
    titleView.font = [[ISSupport defaultManager] headerFont];
    [titleView sizeToFit];
    
    self.navigationItem.titleView = titleView;
    
    self.subjectTitle.text = [ISSupport newTicketSubjectTitle];
    self.subjectName.text = self.subject;
    
    UIBarButtonItem *s = ([ISSupport viewForSendButtonInNewTicketView]) ? [ISSupport viewForSendButtonInNewTicketView] : [[UIBarButtonItem alloc] initWithTitle:[ISSupport newTicketSendButtonTitle]
                                                                                                                                                           style:UIBarButtonItemStylePlain
                                                                                                                                                          target:self
                                                                                                                                                          action:nil];
    if(s.customView != nil && [s.customView isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)s.customView;
        [btn addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [s setTarget:self];
        [s setAction:@selector(send)];
    }
    self.navigationItem.rightBarButtonItem = s;

    self.textInput.layer.masksToBounds = YES;
    self.textInput.layer.cornerRadius = 5.;
    self.textInput.layer.borderWidth = 1.;
    self.textInput.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    if([ISSupport viewForBackButtonInNewTicketView])
    {
        UIBarButtonItem *backButton = [ISSupport viewForBackButtonInNewTicketView];
        if(backButton.customView != nil && [backButton.customView isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)backButton.customView;
            [btn addTarget:self action:@selector(pop) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.leftBarButtonItem = backButton;
        }
    }
    // Do any additional setup after loading the view from its nib.
}

-(void)pop
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - keyboard notifications handle -

-(void)keyboardWillShow:(NSNotification *)info
{
    double animationDuration;
    CGRect rect = [[[info userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    animationDuration = [[[info userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationType = [[[info userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    UIViewAnimationOptions animation = UIViewAnimationOptionCurveLinear;
    
    void (^completition)(void) = ^{
        CGRect inputViewFrame = self.textInput.frame;
        self.textInput.frameHeight = self.view.frameHeight - rect.size.height - 20. - inputViewFrame.origin.y;
	};
    
    if(IS_IOS6_OR_EARLIER)
    {
        switch (animationType) {
            case UIViewAnimationCurveEaseIn:
                animation = UIViewAnimationOptionCurveEaseIn;
                break;
            case UIViewAnimationCurveEaseInOut:
                animation = UIViewAnimationOptionCurveEaseInOut;
                break;
            case UIViewAnimationCurveEaseOut:
                animation = UIViewAnimationOptionCurveEaseOut;
                break;
            case UIViewAnimationCurveLinear:
                animation = UIViewAnimationOptionCurveLinear;
                break;
        }
        [UIView animateWithDuration:animationDuration
                              delay:0.
                            options:animation
                         animations:completition
                         completion:nil];
    }
    else if(IS_IOS7)
    {
        [UIView animateWithDuration:0.5
							  delay:0
			 usingSpringWithDamping:500.0f
			  initialSpringVelocity:0.0f
							options:UIViewAnimationOptionCurveLinear
						 animations:completition
						 completion:nil];
    }
}

-(void)keyboardWillHide:(NSNotification *)info
{
    double animationDuration;
    animationDuration = [[[info userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationType = [[[info userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    UIViewAnimationOptions animation = UIViewAnimationOptionCurveLinear;
    
    self.textInput.contentOffset = CGPointMake(0., 0.);
    void (^completition)(void) = ^{
        CGRect inputViewFrame = self.textInput.frame;
        self.textInput.frameHeight = self.view.frameHeight - 20. - inputViewFrame.origin.y;
    };
    
    if(IS_IOS6_OR_EARLIER)
    {
        switch (animationType) {
            case UIViewAnimationCurveEaseIn:
                animation = UIViewAnimationOptionCurveEaseIn;
                break;
            case UIViewAnimationCurveEaseInOut:
                animation = UIViewAnimationOptionCurveEaseInOut;
                break;
            case UIViewAnimationCurveEaseOut:
                animation = UIViewAnimationOptionCurveEaseOut;
                break;
            case UIViewAnimationCurveLinear:
                animation = UIViewAnimationOptionCurveLinear;
                break;
        }
        [UIView animateWithDuration:animationDuration
                              delay:0.
                            options:animation
                         animations:completition
                         completion:nil];
    }
    else if(IS_IOS7)
    {
        [UIView animateWithDuration:0.5
							  delay:0
			 usingSpringWithDamping:500.0f
			  initialSpringVelocity:0.0f
							options:UIViewAnimationOptionCurveLinear
						 animations:completition
						 completion:nil];
    }
}

#pragma mark - touches handle -

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textInput resignFirstResponder];
}

#pragma mark - send action -

-(void)send
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
     if([self.textInput.text length] > 0 && ![[self.textInput.text stringByTrimmingCharactersInSet:set] length] == 0)
     {
         NSString* URLString = [NSString stringWithFormat:@"%@%@",[ISSupport defaultManager].server,kNewTicketEndpoint];
         NSMutableDictionary *params = [NSMutableDictionary dictionary];
         [params setValue:self.subject forKeyPath:@"title"];
         [params setValue:self.textInput.text forKeyPath:@"message"];
         if([ISSupport defaultManager].deviceToken)
             [params setValue:[ISSupport defaultManager].deviceToken forKey:@"device_id"];

#warning test id
         [params setValue:@"1" forKey:@"device_id"];

         AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
         manager.responseSerializer = [AFJSONResponseSerializer serializer];
         
         [SVProgressHUD showWithStatus:nil maskType:SVProgressHUDMaskTypeGradient];
         [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
          {
              [SVProgressHUD dismiss];
//              if([[responseObject valueForKey:@"status"] isEqualToString:@"ok"])
//              {
                  [self.navigationController popViewControllerAnimated:YES];
//              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [SVProgressHUD showErrorWithStatus:error.localizedDescription];
          }];
     }
     else
     {
         [self.textInput becomeFirstResponder];
     }
}

@end
