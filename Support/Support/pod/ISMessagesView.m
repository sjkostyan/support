//
//  ISMessagesView.m
//  Support
//
//  Created by Сергей Костян on 13.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import "ISMessagesView.h"

#define kTicketMessagesEndpoint    @"/api/ticket/message/list"
#define kTicketNewMessagesEndpoint @"/api/ticket/message/new"

#define kCell @"cell"

@implementation ISTicketMessageCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end


@interface ISMessagesView ()

@end

@implementation ISMessagesView

#pragma mark - status bar -

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return [ISSupport statusBarStyle];
}

#pragma mark - view -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _formatter = [[NSDateFormatter alloc] init];
    [_formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [_formatter setTimeZone:[NSTimeZone timeZoneWithName:@"EEST"]];
    
    _formatterCurrent = [[NSDateFormatter alloc] init];
    [_formatterCurrent setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [_formatterCurrent setTimeZone:[NSTimeZone localTimeZone]];
    
    _messages = [NSMutableArray array];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0., 0., PHFComposeBarViewInitialHeight, 0.);;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    UILabel *titleView = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., 320., 40.)];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [[ISSupport defaultManager] headerTextColor];
    titleView.text = _currentTicket.title;
    titleView.font = [[ISSupport defaultManager] headerFont];
    [titleView sizeToFit];
    
    self.navigationItem.titleView = titleView;
    
    [self _loadMessages];
    
    CGRect viewBounds = [[self view] bounds];
    CGRect frame = CGRectMake(0.0f,
                              viewBounds.size.height - PHFComposeBarViewInitialHeight,
                              viewBounds.size.width,
                              PHFComposeBarViewInitialHeight);
    PHFComposeBarView *composeBarView = [[PHFComposeBarView alloc] initWithFrame:frame];
    for(id v in composeBarView.subviews)
    {
        if([v isKindOfClass:[UIToolbar class]])
        {
            UIToolbar *toolbar = (UIToolbar *)v;
            toolbar.translucent = NO;
            [toolbar setBarTintColor:self.navigationController.navigationBar.barTintColor];
        }
    }
    [composeBarView setButtonTintColor:self.navigationController.navigationBar.tintColor];
    [composeBarView setButtonTitle:[ISSupport ticketMessagesSendButtonTitle]];
    [composeBarView setMaxCharCount:160];
    [composeBarView setMaxLinesCount:1];
    [composeBarView setPlaceholder:[ISSupport ticketMessagesPlaceholder]];
    [composeBarView setUtilityButtonImage:nil];
    [composeBarView setDelegate:self];
    
    _bar = composeBarView;
    
    [self.view addSubview:_bar];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide)];
    tap.delegate = self;
    [self.tableView addGestureRecognizer:tap];
    
    if([ISSupport viewForBackButtonInTicketMessagesView])
    {
        UIBarButtonItem *backButton = [ISSupport viewForBackButtonInTicketMessagesView];
        if(backButton.customView != nil && [backButton.customView isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)backButton.customView;
            [btn addTarget:self action:@selector(pop) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.leftBarButtonItem = backButton;
        }
    }
    // Do any additional setup after loading the view from its nib.
}

-(void)pop
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - keyboard notifications handle -

-(void)keyboardWillShow:(NSNotification *)info
{
    double animationDuration;
    CGRect rect = [[[info userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    animationDuration = [[[info userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationType = [[[info userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    UIViewAnimationOptions animation = UIViewAnimationOptionCurveLinear;
    
    void (^completition)(void) = ^{
        self.tableView.contentInset = UIEdgeInsetsMake(PHFComposeBarViewInitialHeight, 0., (PHFComposeBarViewInitialHeight + rect.size.height), 0.);
        _bar.frameBottom = self.view.frameBottom - rect.size.height;
	};
    
    if(IS_IOS6_OR_EARLIER)
    {
        switch (animationType) {
            case UIViewAnimationCurveEaseIn:
                animation = UIViewAnimationOptionCurveEaseIn;
                break;
            case UIViewAnimationCurveEaseInOut:
                animation = UIViewAnimationOptionCurveEaseInOut;
                break;
            case UIViewAnimationCurveEaseOut:
                animation = UIViewAnimationOptionCurveEaseOut;
                break;
            case UIViewAnimationCurveLinear:
                animation = UIViewAnimationOptionCurveLinear;
                break;
        }
        [UIView animateWithDuration:animationDuration
                              delay:0.
                            options:animation
                         animations:completition
                         completion:nil];
    }
    else if(IS_IOS7)
    {
        [UIView animateWithDuration:0.5
							  delay:0
			 usingSpringWithDamping:500.0f
			  initialSpringVelocity:0.0f
							options:UIViewAnimationOptionCurveLinear
						 animations:completition
						 completion:nil];
    }
}

-(void)keyboardWillHide:(NSNotification *)info
{
    double animationDuration;
    animationDuration = [[[info userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationType = [[[info userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    UIViewAnimationOptions animation = UIViewAnimationOptionCurveLinear;
    
    void (^completition)(void) = ^{
        self.tableView.contentInset = UIEdgeInsetsMake(PHFComposeBarViewInitialHeight, 0., PHFComposeBarViewInitialHeight, 0.);
        _bar.frameBottom = self.view.frameBottom;
    };
    
    if(IS_IOS6_OR_EARLIER)
    {
        switch (animationType) {
            case UIViewAnimationCurveEaseIn:
                animation = UIViewAnimationOptionCurveEaseIn;
                break;
            case UIViewAnimationCurveEaseInOut:
                animation = UIViewAnimationOptionCurveEaseInOut;
                break;
            case UIViewAnimationCurveEaseOut:
                animation = UIViewAnimationOptionCurveEaseOut;
                break;
            case UIViewAnimationCurveLinear:
                animation = UIViewAnimationOptionCurveLinear;
                break;
        }
        [UIView animateWithDuration:animationDuration
                              delay:0.
                            options:animation
                         animations:completition
                         completion:nil];
    }
    else if(IS_IOS7)
    {
        [UIView animateWithDuration:0.5
							  delay:0
			 usingSpringWithDamping:500.0f
			  initialSpringVelocity:0.0f
							options:UIViewAnimationOptionCurveLinear
						 animations:completition
						 completion:nil];
    }
}

#pragma mark - download request -

-(void)_loadMessages
{
    NSString* URLString = [NSString stringWithFormat:@"%@%@",[ISSupport defaultManager].server,kTicketMessagesEndpoint];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if([ISSupport defaultManager].deviceToken)
        [params setValue:[ISSupport defaultManager].deviceToken forKey:@"device_id"];
    if(_currentTicket.ticketID)
        [params setValue:_currentTicket.ticketID forKeyPath:@"ticket_id"];
#warning test id
    [params setValue:@"1" forKey:@"device_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [SVProgressHUD showWithStatus:nil maskType:SVProgressHUDMaskTypeGradient];
    [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [SVProgressHUD dismiss];
         if([[responseObject valueForKey:@"status"] isEqualToString:@"ok"])
         {
             NSArray *messages = [responseObject valueForKey:@"content"];
             NSMutableArray *receivedMessages = [NSMutableArray array];
             if(messages.count)
             {
                 for(NSDictionary *message in messages)
                 {
                     ISTicketMessage *m = [[ISTicketMessage alloc] initWithDictionary:message];
                     if(m)
                         [receivedMessages addObject:m];
                 }
                 _counter = 0;
                 [_messages removeAllObjects];
                 [self sortMessages:receivedMessages];
             }
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [SVProgressHUD showErrorWithStatus:error.localizedDescription];
     }];
}

-(void)sortMessages:(NSArray *)messages
{
    if(_counter < messages.count)
    {
        NSMutableArray *group = [NSMutableArray array];
        NSString *fromUser = [(ISTicketMessage *)messages[_counter] fromUser];
        for(int i = _counter;i<messages.count;i++)
        {
            ISTicketMessage *m = messages[i];
            NSString *fromUserNext = m.fromUser;
            _counter++;
            if([fromUser isEqualToString:fromUserNext])
            {
                [group addObject:m];
            }
            else
            {
                _counter--;
                break;
            }
        }
        if(group.count > 0)
            [_messages addObject:group];
        [self sortMessages:messages];
    }
    else
    {
        [self.tableView reloadData];
        NSInteger sections = [self.tableView numberOfSections];
        NSInteger rows = [self.tableView numberOfRowsInSection:sections-1];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rows-1 inSection:sections-1] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
}

#pragma mark - touches handle -

-(void)hide
{
    [_bar resignFirstResponder];
}

#pragma mark - send button action -

-(void)composeBarViewDidPressButton:(PHFComposeBarView *)composeBarView
{
    [self _sendMessage:[composeBarView text]];
    [composeBarView setText:@""];
    [composeBarView resignFirstResponder];
}

#pragma mark - send request -

-(void)_sendMessage:(NSString *)message
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if([message length] > 0 && ![[message stringByTrimmingCharactersInSet:set] length] == 0)
    {
        NSString* URLString = [NSString stringWithFormat:@"%@%@",[ISSupport defaultManager].server,kTicketNewMessagesEndpoint];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setValue:message forKeyPath:@"message"];
        if([ISSupport defaultManager].deviceToken)
            [params setValue:[ISSupport defaultManager].deviceToken forKey:@"device_id"];
        NSLog(@"ticket %@",_currentTicket.ticketID);
        if(_currentTicket.ticketID)
            [params setValue:_currentTicket.ticketID forKeyPath:@"ticket_id"];
#warning test id
        [params setValue:@"1" forKey:@"device_id"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [SVProgressHUD showWithStatus:nil maskType:SVProgressHUDMaskTypeGradient];
        [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [SVProgressHUD dismiss];
             if([[responseObject valueForKey:@"status"] isEqualToString:@"ok"])
             {
                 [self _loadMessages];
                 //             NSArray *tickets = [responseObject valueForKey:@"content"];
                 //             NSMutableArray *receivedTickets = [NSMutableArray array];
                 //             if(tickets.count)
                 //             {
                 //                 for(NSDictionary *ticket in tickets)
                 //                 {
                 //                     ISTicket *t = [[ISTicket alloc] initWithDictionary:ticket];
                 //                     if(t)
                 //                         [receivedTickets addObject:t];
                 //                 }
                 //                 _tickets = receivedTickets;
                 //             }
             }
             //         [self.tableView reloadData];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [SVProgressHUD showErrorWithStatus:error.localizedDescription];
#warning hot fix
             [self _loadMessages];
         }];
    }
}

#pragma mark - Table view data source -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _messages.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_messages objectAtIndex:section] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ISTicketMessage *m = _messages[indexPath.section][indexPath.row];
    
    UILabel *messageLbl = [[UILabel alloc] init];
    messageLbl.frameSize = CGSizeMake(tableView.frameWidth - 20., FLT_MAX);
    messageLbl.numberOfLines = 0.;
    messageLbl.text = m.text;
    
    [messageLbl sizeToFit];
    
    return 34. + messageLbl.frameHeight + 10.;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ISTicketMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:kCell];
    if(!cell)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ISMessageCell" owner:self options:nil] lastObject];
    }
    
    ISTicketMessage *m = _messages[indexPath.section][indexPath.row];
    
    cell.message.frameSize = CGSizeMake(tableView.frameWidth - 20., FLT_MAX);
    cell.message.text = m.text;
    [cell.message sizeToFit];
    cell.message.frameY = 34.;
    
    NSDate *d = [_formatter dateFromString:m.createdAt];
    
    cell.date.text = [_formatterCurrent stringFromDate:d];
    if([m.fromUser isEqualToString:@"0"])
    {
        cell.from.text = [ISSupport administratorDisplayName];
        cell.icon.image = [[UIImage imageNamed:@"is_moderator"] changeColor:[UIColor lightGrayColor]];
    }
    else {
        cell.from.text = [ISSupport userDisplayName];
        cell.icon.image = [[UIImage imageNamed:@"is_guest"] changeColor:[UIColor lightGrayColor]];
    }
    
//    cell.textLabel.text = [NSString stringWithFormat:@"%ld-%ld",indexPath.section,indexPath.row];
    
    return cell;
}

@end
