//
//  ISMessage.h
//  Support
//
//  Created by Сергей Костян on 13.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISTicketMessage : NSObject

@property (strong) NSString *ticketID;
@property (strong) NSString *messageID;
@property (strong) NSString *text;
@property (strong) NSString *fromUser;
@property (strong) NSString *createdAt;

-(id)initWithDictionary:(NSDictionary *)object;

@end
