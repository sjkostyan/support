//
//  ISTicketForm.h
//  Support
//
//  Created by Сергей Костян on 12.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISSupportHeaders.h"

@interface ISTicketForm : UIViewController <UITextViewDelegate>

@property (strong) NSString *subject;

@property (strong) IBOutlet UILabel *subjectTitle;
@property (strong) IBOutlet UILabel *subjectName;
@property (strong) IBOutlet UITextView  *textInput;

@end
