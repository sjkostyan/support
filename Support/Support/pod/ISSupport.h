//
//  SKDataManager.h
//  MensLook
//
//  Created by Сергей Костян on 23.04.14.
//  Copyright (c) 2014 Сергей Костян. All rights reserved.
//
#define IS_IOS6_OR_EARLIER ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
#define IS_IOS7            ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)

#import <Foundation/Foundation.h>
#import "ISTicketsVC.h"

@protocol ISSupportUIDelegate;

@interface ISSupport : NSObject
{
    ISTicketsVC *_ticketVC;
    UIFont *_headerFont;
    UIColor *_headerTextColor;
}

@property (weak) id<ISSupportUIDelegate> delegate;

@property (strong) NSString *server;
@property (strong) NSString *deviceToken;

#pragma mark - singleton -

+(ISSupport *)defaultManager;

-(ISTicketsVC *)ticketVC;
-(NSSet *)subjects;

#pragma mark  - customization

#pragma mark - font -
-(void)setHeaderFont:(UIFont *)font;
-(UIFont *)headerFont;

#pragma mark - color -
-(void)setHeaderTextColor:(UIColor *)color;
-(UIColor *)headerTextColor;

#pragma mark - status bar -
+(void)statusBarStyle:(UIStatusBarStyle)style;
+(UIStatusBarStyle)statusBarStyle;

#pragma mark - titles -
+(void)setTicketsListHeader:(NSString *)title;
+(void)setAlertTitle:(NSString *)title;
+(void)setAlertMessage:(NSString *)title;
+(void)setAlertCancelButtonTitle:(NSString *)title;
+(void)setNewTicketBackButtonTitle:(NSString *)title;
+(void)setNewTicketSendButtonTitle:(NSString *)title;
+(void)setNewTicketSubjectTitle:(NSString *)title;
+(void)setTicketMessagesBackButtonTitle:(NSString *)title;
+(void)setTicketMessagesSendButtonTitle:(NSString *)title;
+(void)setTicketMessagesPlaceholder:(NSString *)title;
+(void)setAdministratorDisplayName:(NSString *)title;
+(void)setUserDisplayName:(NSString *)title;

#pragma mark - customization getters -
+(NSString *)ticketsListHeader;
+(NSString *)alertTitle;
+(NSString *)alertMessage;
+(NSString *)alertCancelButtonTitle;
+(NSString *)newTicketBackButtonTitle;
+(NSString *)newTicketSendButtonTitle;
+(NSString *)newTicketSubjectTitle;
+(NSString *)ticketMessagesBackButtonTitle;
+(NSString *)ticketMessagesSendButtonTitle;
+(NSString *)ticketMessagesPlaceholder;
+(NSString *)administratorDisplayName;
+(NSString *)userDisplayName;

+(UIBarButtonItem *)viewForAddButton;
+(UIBarButtonItem *)viewForBackButtonInNewTicketView;
+(UIBarButtonItem *)viewForSendButtonInNewTicketView;
+(UIBarButtonItem *)viewForBackButtonInTicketMessagesView;
//+(UIButton *)viewForSendButtonInTicketMessagesView;

@end

#pragma mark - customization UI delegate -

/*
 
 Yopu don't need to set selectors and target. 
 
 */

@protocol ISSupportUIDelegate <NSObject>

@optional
-(UIBarButtonItem *)viewForAddButton;
-(UIBarButtonItem *)viewForBackButtonInNewTicketView;
-(UIBarButtonItem *)viewForSendButtonInNewTicketView;
-(UIBarButtonItem *)viewForBackButtonInTicketMessagesView;
//-(UIButton *)viewForSendButtonInTicketMessagesView;

@end
