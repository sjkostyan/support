
Pod::Spec.new do |s|
  s.name         = "ISSupport"
  s.version      = "1.0.0"
  s.summary      = "ISSupport is a pod for communication inside up with support team."

  s.description  = <<-DESC
                   A longer description of ISSupport in Markdown format.

                   * Think: Why did you write this? What is the focus? What does it do?
                   * CocoaPods will be using this to generate tags, and improve search results.
                   * Try to keep it short, snappy and to the point.
                   * Finally, don't worry about the indent, CocoaPods strips it!
                   DESC

  s.homepage     = "http://bitbucket.org/sjkostyan/support"
  s.license      = "MIT"
  s.author       = { "Сергей Костян" => "sjsoad666@yandex.ru" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://bitbucket.org/sjkostyan/support.git", :tag => s.version.to_s }
  s.resources = ["Support/Support/design/*.*", "Support/Support/*.{xib}"]
  s.source_files = "Support/Support/pod/*.{h,m}"
  s.requires_arc = true
  s.prefix_header_contents = ["#import <AFNetworking.h>" ,"#import <SVProgressHUD.h>"]
  s.dependency "SVProgressHUD", "~> 1.0"
  s.dependency "AFNetworking", "~> 2.0.3"
  s.dependency "PHFComposeBarView", "~> 2.0.1"

end
