//
//  ISTicketMessageCell.h
//  Support
//
//  Created by Сергей Костян on 16.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

//#import <UIKit/UIKit.h>
//
//@interface ISTicketMessageCell : UITableViewCell
//
//@property (strong) IBOutlet UILabel *from;
//@property (strong) IBOutlet UILabel *date;
//@property (strong) IBOutlet UILabel *message;
//@property (strong) IBOutlet UIImageView *icon;
//
//@end
